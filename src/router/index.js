import Vue from 'vue';
import Router from 'vue-router';

import Dashboard from '@/components/Dashboard';
import DashboardContent from '@/components/DashboardContent';
import Tags from '@/components/Tags';

import Folders from '@/components/Folders';

import SignUp from '@/components/SignUp';
import SignIn from '@/components/SignIn';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: './dashboard',
    },
    {
      path: '/dashboard',
      // name: 'dashboard',
      component: Dashboard,
      children: [
        {
          path: '/',
          name: 'dashboardcontent',
          component: DashboardContent,
        },
        {
          path: 'folders',
          name: 'folders',
          component: Folders,
        },
        {
          path: 'tags',
          name: 'tags',
          component: Tags,
        },
      ],
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignUp,
    },
    {
      path: '/signin',
      name: 'signin',
      component: SignIn,
    },
  ],
});
